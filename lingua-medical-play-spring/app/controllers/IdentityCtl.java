package controllers;


import models.Identity;

import org.springframework.beans.factory.annotation.Autowired;

import play.mvc.BodyParser;
import play.mvc.BodyParser.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.IdentityServiceImpl;

@org.springframework.stereotype.Controller
public class IdentityCtl extends Controller{

    @Autowired
    private IdentityServiceImpl identityService;
    
	@BodyParser.Of(Json.class)
	 public Result create() {
		Identity identity = new Identity();
		identity.setFirstName(request().body().asJson().findPath("firstName").asText());
		identity.setLastName(request().body().asJson().findPath("lastName").asText());
		identity.setEmail(request().body().asJson().findPath("email").asText());
		identity.setUserName(request().body().asJson().findPath("userName").asText());
		identity.setPassword(request().body().asJson().findPath("password").asText());
		//Response response = ControllerValidator.validateIdentity(identity);
		//return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	    //return (!response.getErrors().isEmpty()) ? play.mvc.Controller.ok(play.libs.Json.toJson(response)):
	    								//play.mvc.Controller.ok(play.libs.Json.toJson(identityService.create(identity))); 
		return play.mvc.Controller.ok(play.libs.Json.toJson(identityService.create(identity)));
	 }
}