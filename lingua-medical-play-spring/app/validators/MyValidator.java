package validators;

import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;

import models.Identity;
import domain.Response;
import domain.ResponseType;

public class MyValidator  {
   private MyValidator(){}
 
	public static Response validateIdentity(@Valid final Identity identity) {
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		return to(validator.validate(identity, Default.class));
	}
	
	public static Response to(final Set<ConstraintViolation<Identity>> violatedConstraints) {
		Response response = new Response();
		if (!violatedConstraints.isEmpty()) {
			response.setCode(ResponseType.CONSTRAINT_VALIDATION_EXCEPTION.getValue());
			response.setClassfication(ResponseType.CONSTRAINT_VALIDATION_EXCEPTION.name());
			Iterator<ConstraintViolation<Identity>> iterateThroughConstraints = violatedConstraints.iterator();
			
			while (iterateThroughConstraints.hasNext()) {
				ConstraintViolation<Identity> violation =iterateThroughConstraints.next();
				response.getErrors().put(violation.getPropertyPath().toString(),violation.getMessage());
			}

		} else {
			response.setCode(19);
		}
		
		return response;
	}
	public static Response _constraintToResponse(final Set<ConstraintViolation<?>> violatedConstraints) {
		Response response = new Response();
		if (!violatedConstraints.isEmpty()) {
			response.setCode(ResponseType.CONSTRAINT_VALIDATION_EXCEPTION.getValue());
			response.setClassfication(ResponseType.CONSTRAINT_VALIDATION_EXCEPTION.name());
			Iterator<ConstraintViolation<?>> iterateThroughConstraints = violatedConstraints.iterator();
			
			while (iterateThroughConstraints.hasNext()) {
				ConstraintViolation<?> violation =iterateThroughConstraints.next();
				response.getErrors().put(violation.getPropertyPath().toString(),violation.getMessage());
			}

		} else {
			response.setCode(19);
		}
		return response;
	}
}
