package repositories;

import domain.Response;
import models.Identity;

public interface IdentityDAO {
	Response create(Identity identity);
}
