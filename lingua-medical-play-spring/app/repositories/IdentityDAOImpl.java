package repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.transaction.annotation.Transactional;

import models.Identity;

import org.springframework.stereotype.Repository;


@Repository
@Transactional
public class IdentityDAOImpl{

    @PersistenceContext
    EntityManager em;

	public Long create(Identity identity) throws ConstraintViolationException{
		em.persist(identity);
		em.flush();
		return identity.getPidm();
	}
}
