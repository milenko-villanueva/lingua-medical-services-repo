package services;


import java.sql.Timestamp;
import java.util.UUID;

import javax.validation.ConstraintViolationException;

import models.Identity;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import repositories.IdentityDAOImpl;
import validators.MyValidator;
import domain.IdentityStatus;
import domain.IdentityType;
import domain.Response;
import domain.ResponseType;
    //TODO 0.Validate email
	//TODO 1.Generate password
	//TODO 2.Generate unique id;
	//TODO 3.Generate unique pidm.
    //TODO:ConstraintViolationException:
    //TODO:Implement Identity Builder Pattern.
@Service
public class IdentityServiceImpl{
	@Autowired
	private IdentityDAOImpl identityDAO;
	private static final int ZERO=0;
	
	public Response create(Identity identity) {
		Response response = new Response();
		   try{
			identity.setId(UUID.randomUUID().toString());
			identity.setOrigin(getClass().getSimpleName());
			identity.setStatus(IdentityStatus.ACTIVE.getVal());
			identity.setType(IdentityType.STUDENT.getVal());
			identity.setCreated(new Timestamp(new DateTime().getMillis()));
			
		    if (identityDAO.create(identity)>ZERO){
		    	response.setCode(ResponseType.OK.getValue());
		    	response.setClassfication(ResponseType.OK.name());
		    }else{
		    	response.setCode(ResponseType.CREATE_IDENTITY_FAILURE.getValue());
		    	response.setClassfication(ResponseType.CREATE_IDENTITY_FAILURE.name());
		    }
		   }catch(ConstraintViolationException e){
			 response = MyValidator._constraintToResponse(e.getConstraintViolations());
		   }
		 return response;
	}	
}