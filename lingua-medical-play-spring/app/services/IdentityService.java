package services;

import models.Identity;
import domain.Response;

public interface IdentityService {
	Response create(Identity identity);
}
