package models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.groups.Default;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;

@Entity
@Table(name="identity"/*, schema="lingua"*/)
public class Identity {
	   
	   private String id;
	   @Id
	   @GeneratedValue
	   @Column(name = "pidm")
	   private  Long pidm;
	   @Required(message="firstName is required",groups=Default.class)
	   @Column(name = "firstname")
	   private String firstName;
	   @Required(message="lastName is required",groups=Default.class)
	   @Column(name = "lastname")
	   private String lastName;
	   @Required(message="userName is required",groups=Default.class)
	   @Column(name = "username")
	   private String userName;
	   private String password;
	   @Required(message="email is required",groups=Default.class)
	   @Email(message="Invalid Email Format")
	   private String email;
	   private String changed;
	   private String type;
	   private int status;
	   private String origin;
	   private Timestamp created;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the pidm
	 */
	public Long getPidm() {
		return pidm;
	}
	/**
	 * @param pidm the pidm to set
	 */
	public void setPidm(Long pidm) {
		this.pidm = pidm;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the changed
	 */
	public String getChanged() {
		return changed;
	}
	/**
	 * @param changed the changed to set
	 */
	public void setChanged(String changed) {
		this.changed = changed;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}
	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	/**
	 * @return the created
	 */
	public Timestamp getCreated() {
		return created;
	}
	/**
	 * @param created the created to set
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
}
