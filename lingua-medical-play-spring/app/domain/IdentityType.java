package domain;

public enum IdentityType {
	STUDENT("S");
	String val;
	IdentityType(String val){
		this.val = val;
	}
	public String getVal(){
		return this.val;
	}
}
