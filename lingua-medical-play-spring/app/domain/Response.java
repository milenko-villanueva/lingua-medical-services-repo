package domain;

import java.util.HashMap;
import java.util.Map;

public class Response {
	private int code;
	private String classfication;
	private Map<String,String> errors = new HashMap<String,String>();
	
	public Response(){}
	public Response (int code, String classification){
		this.code = code;
		this.classfication = classification;
	}
	public Response(int code, String classification, Map<String, String> errors) {
		this.code = code;
		this.classfication = classification;
		this.errors = errors;
	}
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	/**
	 * @return the errors
	 */
	public Map<String, String> getErrors() {
		return errors;
	}
	/**
	 * @param errors the errors to set
	 */
	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}
	/**
	 * @return the classfication
	 */
	public String getClassfication() {
		return classfication;
	}
	/**
	 * @param classfication the classfication to set
	 */
	public void setClassfication(String classfication) {
		this.classfication = classfication;
	}
	
				
}

