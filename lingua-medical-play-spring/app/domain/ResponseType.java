package domain;

public enum ResponseType {
	OK(1),
	CREATE_IDENTITY_FAILURE(801),
	CONSTRAINT_VALIDATION_EXCEPTION(901);
	private int value;	
	ResponseType(int value){
		this.value = value;
	}
	public int getValue(){
		return this.value;
	}

}
