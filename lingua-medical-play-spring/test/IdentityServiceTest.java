import static org.fest.assertions.Assertions.assertThat;
import models.Identity;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import services.IdentityServiceImpl;
import configs.AppConfig;
import domain.Response;
import domain.ResponseType;

@ContextConfiguration(classes={AppConfig.class, TestDataConfig.class})
public class IdentityServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private IdentityServiceImpl identityService;

    @Test
    public void create() {
    	 Identity identity = new Identity();
         identity.setFirstName("Milenko");
         identity.setLastName("Villanueva");
         identity.setUserName("milenko.villanueva@gmail.com");
         identity.setPassword("Masalachol0573$");
         identity.setEmail("milenko.villanueva@gmail.com");
    	 Response response = identityService.create(identity);
    	 assertThat(response.getCode()).isEqualTo(ResponseType.OK.getValue());
    }
    //TODO: Need to create a method that retreives the identity.
}