CREATE SCHEMA lingua AUTHORIZATION SA;
create table lingua.identity (                
        id varchar(60),
        pidm bigint not null AUTO_INCREMENT PRIMARY KEY,                
        firstname varchar(50) not null,
        lastname varchar(50) not null,
        username varchar(50) not null,
        password varchar(255) not null,        
        email varchar(125) not null,
        changed varchar(1),
        type varchar(4),
        status int,
        origin varchar(100),
        created timestamp DEFAULT CURRENT_TIMESTAMP
    ) 