import static org.fest.assertions.Assertions.assertThat;

import java.sql.Timestamp;

import models.Identity;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import configs.AppConfig;

@ContextConfiguration(classes={AppConfig.class, TestDataConfig.class})
public class ModelTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Test
    public void identityMembers() {
         Identity identity = new Identity();
         identity.setFirstName("Milenko");
         identity.setLastName("Villanueva");
         identity.setUserName("milenko.villanueva@gmail.com");
         identity.setPassword("Masalachol0573$");
         identity.setEmail("milenko.villanueva@gmail.com");
         identity.setId("1234513213");
         identity.setPidm(new Long(12345));
         identity.setChanged(null);
         identity.setCreated(new Timestamp(new DateTime().getMillis()));
         identity.setOrigin("identity.create");
         
         assertThat(identity.getFirstName()).isEqualTo("Milenko");
         assertThat(identity.getLastName()).isEqualTo("Villanueva");
         assertThat(identity.getUserName()).isEqualTo("milenko.villanueva@gmail.com");
         assertThat(identity.getPassword()).isEqualTo("Masalachol0573$");
         assertThat(identity.getEmail()).isEqualTo("milenko.villanueva@gmail.com");
         assertThat(identity.getId()).isEqualTo("1234513213");
         assertThat(identity.getPidm().equals(new Long(12345)));
         assertThat(identity.getChanged()).isNull();;
         assertThat(identity.getCreated()).isNotNull();
         assertThat(identity.getOrigin()).isEqualTo("identity.create");
    }

}
